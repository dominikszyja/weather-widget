import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export const baseUrl = 'http://dev-weather-api.azurewebsites.net';
export const WEATHER_TYPE = {
  RainAndCloudy: 'rain_s_cloudy.png',
  RainLight: 'rain_light.png',
  Cloudy: 'cloudy.png',
  PartlyCloudy: 'partly_cloudy.png',
  Sunny: 'sunny.png'
};

export interface WindModel {
  speed: number;
  direction: string;
}

export interface WeatherModel {
  cityId: string;
  date: string;
  temperature: number;
  precipitation: number;
  humidity: number;
  windInfo: WindModel;
  pollenCount: number;
  type: string;
}

export interface CityModel {
  id: string;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  url = '/api/city';
  constructor(private httpClient: HttpClient) {
    this.url = baseUrl + this.url;
  }

  getCitiesForDropdown(): Observable<Array<CityModel>> {
    return this.httpClient.get<Array<CityModel>>(this.url);
  }

  getWeather(cityId, date = null): Observable<Array<WeatherModel>> {
    if (cityId) {
      if (!date) {
        date = new Date().toJSON();
      }
      return this.httpClient.get<Array<WeatherModel>>(`${this.url}/${cityId}/weather?date=${date}`);
    }
  }
}
