import { Component, OnInit } from '@angular/core';
import { WeatherService, WEATHER_TYPE, WeatherModel, CityModel } from '../weather.service';
import { MOCK_API_RESPONSE } from 'src/mock-data';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {
  imageURL = 'assets/img/';

  weatherData: Array<WeatherModel>;
  citiesData: Array<CityModel>;
  currentDay: WeatherModel;
  currentCityId: string;

  isLoading = true;

  constructor(private weatherService: WeatherService) {

  }

  getWeatherImageName(type: string) {
    return (type in WEATHER_TYPE && WEATHER_TYPE[type]) ? WEATHER_TYPE[type] : 'blank.png';
  }

  public trackByFn(index, item) {
    return item ? item.date : null;
  }

  public trackCityByFn(index, item) {
    return item ? item.date : null;
  }

  makeRequest(cityId) {
    if (cityId) {
      this.weatherService.getWeather(cityId).subscribe(response => {
        if (response) {
          this.weatherData = response;
          this.currentDay = this.weatherData.shift();
        }
      }, err => console.log(err.error), () => {
        this.isLoading = false;
      });
    }
  }

  ngOnInit() {
    this.weatherService.getCitiesForDropdown().subscribe(response => {
      if (response && response.length > 0) {
        this.citiesData = response;
        this.currentCityId = response[0].id;

        this.makeRequest(this.currentCityId);
      }
    });

  }
}
