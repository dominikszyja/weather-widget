export const MOCK_API_RESPONSE = [{
  cityId: '1', date: '2019-07-25T00:00:00', temperature: 15, precipitation: 80, humidity: 40,
  windInfo: { speed: 7, direction: 'S' },
  pollenCount: 21, type: 'RainAndCloudy'
}, {
  cityId: '1', date: '2019-07-26T00:00:00', temperature: 15, precipitation: 80, humidity: 40,
  windInfo: { speed: 7, direction: 'N' },
  pollenCount: 21, type: 'RainLight'
}, {
  cityId: '1', date: '2019-07-27T00:00:00', temperature: -1, precipitation: 50, humidity: 60,
  windInfo: { speed: 7, direction: 'S' },
  pollenCount: 11, type: 'Cloudy'
}, {
  cityId: '1', date: '2019-07-28T00:00:00', temperature: 20, precipitation: 30, humidity: 20,
  windInfo: { speed: 7, direction: 'N' },
  pollenCount: 56, type: 'RainLight'
}, {
  cityId: '1', date: '2019-07-29T00:00:00', temperature: -15, precipitation: 80, humidity: 40,
  windInfo: { speed: 7, direction: 'S' },
  pollenCount: 21, type: 'RainAndCloudy'
}, {
  cityId: '1', date: '2019-07-28T00:00:00', temperature: 20, precipitation: 30, humidity: 20,
  windInfo: { speed: 7, direction: 'N' },
  pollenCount: 56, type: 'RainLight'
}, {
  cityId: '1', date: '2019-07-29T00:00:00', temperature: -15, precipitation: 80, humidity: 40,
  windInfo: { speed: 7, direction: 'S' },
  pollenCount: 21, type: 'RainAndCloudy'
}];


