# WeatherApp

Zadanie testowe stworzenie dynamicznego widgetu przedstawiającego obecną pogodę. Zrealizowane w postaci komponentu, serwisu

Wersja na żywo:

[WeatherApp](https://domszyja.bitbucket.io/weather/) (https://domszyja.bitbucket.io/weather/)

## Zastosowanie:

Widget podpięty jako komponent '<app-widget></app-widget>' dodatkowo zostały rozpisane modele na podstawie otrzymanych informacji z serwera 'WindModel', 'WeatherModel', 'CityModel' które znajdują się w serwisie *weather.service.ts*

## Urchomienie lokalnie

``` ng serve ```
